﻿namespace Main
{
    partial class FightScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FightScreen));
            this.playerPicture = new System.Windows.Forms.PictureBox();
            this.enemyPicture = new System.Windows.Forms.PictureBox();
            this.attackBtn = new System.Windows.Forms.Button();
            this.playerHealthDisplay = new System.Windows.Forms.TextBox();
            this.enemyHealthDisplay = new System.Windows.Forms.TextBox();
            this.playerNameLabel = new System.Windows.Forms.Label();
            this.enemyNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.playerPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemyPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // playerPicture
            // 
            this.playerPicture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.playerPicture.BackColor = System.Drawing.Color.Transparent;
            this.playerPicture.Cursor = System.Windows.Forms.Cursors.No;
            this.playerPicture.Image = ((System.Drawing.Image)(resources.GetObject("playerPicture.Image")));
            this.playerPicture.Location = new System.Drawing.Point(12, 35);
            this.playerPicture.Name = "playerPicture";
            this.playerPicture.Size = new System.Drawing.Size(149, 138);
            this.playerPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.playerPicture.TabIndex = 0;
            this.playerPicture.TabStop = false;
            // 
            // enemyPicture
            // 
            this.enemyPicture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.enemyPicture.BackColor = System.Drawing.Color.Transparent;
            this.enemyPicture.Cursor = System.Windows.Forms.Cursors.No;
            this.enemyPicture.Image = ((System.Drawing.Image)(resources.GetObject("enemyPicture.Image")));
            this.enemyPicture.Location = new System.Drawing.Point(388, 35);
            this.enemyPicture.Name = "enemyPicture";
            this.enemyPicture.Size = new System.Drawing.Size(149, 138);
            this.enemyPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.enemyPicture.TabIndex = 1;
            this.enemyPicture.TabStop = false;
            // 
            // attackBtn
            // 
            this.attackBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.attackBtn.Location = new System.Drawing.Point(12, 209);
            this.attackBtn.Name = "attackBtn";
            this.attackBtn.Size = new System.Drawing.Size(525, 28);
            this.attackBtn.TabIndex = 2;
            this.attackBtn.Text = "Attack";
            this.attackBtn.UseVisualStyleBackColor = true;
            this.attackBtn.Click += new System.EventHandler(this.attackBtn_Click);
            // 
            // playerHealthDisplay
            // 
            this.playerHealthDisplay.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.playerHealthDisplay.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.playerHealthDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.playerHealthDisplay.Location = new System.Drawing.Point(12, 180);
            this.playerHealthDisplay.Name = "playerHealthDisplay";
            this.playerHealthDisplay.ReadOnly = true;
            this.playerHealthDisplay.Size = new System.Drawing.Size(149, 25);
            this.playerHealthDisplay.TabIndex = 3;
            // 
            // enemyHealthDisplay
            // 
            this.enemyHealthDisplay.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.enemyHealthDisplay.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.enemyHealthDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.enemyHealthDisplay.Location = new System.Drawing.Point(388, 180);
            this.enemyHealthDisplay.Name = "enemyHealthDisplay";
            this.enemyHealthDisplay.ReadOnly = true;
            this.enemyHealthDisplay.Size = new System.Drawing.Size(149, 25);
            this.enemyHealthDisplay.TabIndex = 4;
            // 
            // playerNameLabel
            // 
            this.playerNameLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.playerNameLabel.AutoSize = true;
            this.playerNameLabel.Location = new System.Drawing.Point(12, 13);
            this.playerNameLabel.Name = "playerNameLabel";
            this.playerNameLabel.Size = new System.Drawing.Size(42, 18);
            this.playerNameLabel.TabIndex = 5;
            this.playerNameLabel.Text = "label1";
            // 
            // enemyNameLabel
            // 
            this.enemyNameLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.enemyNameLabel.AutoSize = true;
            this.enemyNameLabel.Location = new System.Drawing.Point(385, 13);
            this.enemyNameLabel.Name = "enemyNameLabel";
            this.enemyNameLabel.Size = new System.Drawing.Size(42, 18);
            this.enemyNameLabel.TabIndex = 6;
            this.enemyNameLabel.Text = "label1";
            // 
            // FightScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.ClientSize = new System.Drawing.Size(549, 243);
            this.Controls.Add(this.enemyNameLabel);
            this.Controls.Add(this.playerNameLabel);
            this.Controls.Add(this.enemyHealthDisplay);
            this.Controls.Add(this.playerHealthDisplay);
            this.Controls.Add(this.attackBtn);
            this.Controls.Add(this.enemyPicture);
            this.Controls.Add(this.playerPicture);
            this.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FightScreen";
            this.Text = "FightScreen";
            ((System.ComponentModel.ISupportInitialize)(this.playerPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemyPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox playerPicture;
        private System.Windows.Forms.PictureBox enemyPicture;
        private System.Windows.Forms.Button attackBtn;
        private System.Windows.Forms.TextBox playerHealthDisplay;
        private System.Windows.Forms.TextBox enemyHealthDisplay;
        private System.Windows.Forms.Label playerNameLabel;
        private System.Windows.Forms.Label enemyNameLabel;
    }
}