﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    enum InventorySlotId
    {
        UNEQUIPPABLE = 0,
        HELMET = 1,
        CHESTPIECE = 2,
        GRIEVES = 3,
        VAMBRACES = 4,
        WEAPON = 5,
        POTION1 = 6,
        POTION2 = 7
    }
}
