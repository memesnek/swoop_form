﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    class Item : IComparable
    {
        private Guid _id = new Guid();
        private string _name;
        private double _weight;
        private InventorySlotId _slot;
        private bool _isNatural;

        public Guid Id { get { return _id; } }
        public string Name { get { return _name; } }
        public double Weight { get { return _weight; } }
        public InventorySlotId Slot { get { return _slot; } }
        public bool IsNatural { get { return _isNatural; } }

        public int CompareTo(object obj)
        {
            if (this.GetHashCode() > obj.GetHashCode())
            {
                return 1;
            }
            else if (this.GetHashCode() == obj.GetHashCode())
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
        public override bool Equals(object obj)
        {
            if (this.GetHashCode() == obj.GetHashCode())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return _name + " " +
                "W: " + _weight +
                " " + _slot;
        }
    }
}
