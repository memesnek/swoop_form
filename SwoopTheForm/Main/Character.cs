﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    class Character
    {
        protected StoredItems _bag;
        protected EquippedItems _equipped;
        protected string _name;
        protected int _totalHealth;
        protected int _currentHealth;
        protected bool _dead;

        public Character(string name, int health)
        {
            _name = name;
            _totalHealth = health;
            _currentHealth = _totalHealth;
            _bag = new StoredItems(20);
            _equipped = new EquippedItems();
        }

        public StoredItems Bag { get { return _bag; } }
        public EquippedItems Equipped { get { return _equipped; } }
        public string Name { get { return _name; } }
        public int TotalHealth { get { return _totalHealth; } }
        public int CurrentHealth { get { return _currentHealth; } }
        public bool IsDead
        {
            get
            {
                //if the character's health is below 0 it is set to 0 to avoid errors
                if (_currentHealth <= 0)
                {
                    _currentHealth = 0;
                    _dead = true;
                }
                else
                {
                    _dead = false;
                }
                return _dead;
            }
        }

        public double CalcTotalWeight()
        {
            return Bag.CalcTotalWeight() + Equipped.CalcTotalWeight();
        }
        public int CalcTotalAttackValue()
        {
            return 5;   // FIXME: + _equipped.CalcTotalAttackValue()
        }
        public int CalcTotalDefenseValue()
        {
            return 2;   // FIXME: + _equipped.CalcTotalDefenseValue()
        }
        public void TakeDamage(int damage)
        {
            if (!_dead) //prevents character from taking damage if they are dead.
            {
                _currentHealth -= damage;

                if (_currentHealth <= 0)//if the character is killed by the attack health is set to 0 to prevent errors from negative numbers.
                {
                    _currentHealth = 0;
                    _dead = true;
                }
            }
        }
        public void Pickup(Item item)
        {
            _bag.AddItem(item);
        }
        public void UnequipAll()
        {

        }
    }
}
