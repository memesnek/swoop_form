﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    class StoredItems
    {
        private Item[] _items;
        private int _count;

        public int Count { get { return _count; } }
        public int Capacity { get { return 20; } } // FIXME: return _items.Length

        public StoredItems(int size)
        {
            _items = new Item[size];
            // FIXME: _count = 0;
        }

        public Item GetItem(int index)
        {
            return _items[index];
        }

        //public Item SetItem(int index, Item item)
        //{

        //}

        public void AddItem(Item item)
        {
            _items[_count] = item;
            _count++;
        }

        public void RemoveItem(Item item)
        {

        }

        public double CalcTotalWeight()
        {
            double weight = 0;
            for (int i = 0; i < _items.Length; i++)
            {
                weight += _items[i].Weight;
            }
            return weight;
        }
    }
}
