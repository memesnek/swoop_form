﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    class GameManager
    {
        private string[] _enemy_Array = new string[4] { "poking man", "slashing man", "scary man", "Boss Man" };
        private Character _player;
        private Character _enemy;
        private int _depth;
        private bool _gameOver;

        public GameManager()
        {
            //creates the player character and the first enemy
            _player = new Character("Player", 100);
            NewEnemy();
        }

        // FIXME: rename Player
        public Character GetPlayerCharacter { get { return _player; } }
        // FIXME: rename Enemy
        public Character GetEnemyCharacter { get { return _enemy; } }

        //makes a random number generator
        //FIXME: don't make this static
        static Random random = new Random();

        public void NewEnemy()
        {
            _depth++;
            //selects a random number from 0 to 3 and chooses an enemy based off of the number
            int randomNum = random.Next(0, 4);
            switch (randomNum)
            {
                case 0:
                    _enemy = new Character(_enemy_Array[randomNum], 20);
                    break;
                case 1:
                    _enemy = new Character(_enemy_Array[randomNum], 50);
                    break;
                case 2:
                    _enemy = new Character(_enemy_Array[randomNum], 130);
                    break;
                case 3:
                    _enemy = new Character(_enemy_Array[randomNum], 200);
                    break;
                default:
                    _enemy = new Character(_enemy_Array[0], 20);
                    break;
            }
        }
        //public void NewItem()
        //{
        //    int randomNum = random.Next(0, 4);

        //}
    }
}
