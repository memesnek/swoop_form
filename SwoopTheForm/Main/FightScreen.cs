﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class FightScreen : Form
    {
        GameManager gameManager = new GameManager();
        public FightScreen()
        {
            InitializeComponent();
            //make the hud display the proper info
            enemyHealthDisplay.Text = gameManager.GetEnemyCharacter.CurrentHealth.ToString() + "/" + gameManager.GetEnemyCharacter.TotalHealth;
            playerHealthDisplay.Text = gameManager.GetPlayerCharacter.CurrentHealth.ToString() + "/" + gameManager.GetPlayerCharacter.TotalHealth;
            playerNameLabel.Text = gameManager.GetPlayerCharacter.Name;
            enemyNameLabel.Text = gameManager.GetEnemyCharacter.Name;
        }

        private void attackBtn_Click(object sender, EventArgs e)
        {
            if (!gameManager.GetPlayerCharacter.IsDead && !gameManager.GetEnemyCharacter.IsDead) //if neither character is dead do an attack tick
            {
                gameManager.GetEnemyCharacter.TakeDamage(gameManager.GetPlayerCharacter.CalcTotalAttackValue() - gameManager.GetEnemyCharacter.CalcTotalDefenseValue());
                enemyHealthDisplay.Text = gameManager.GetEnemyCharacter.CurrentHealth.ToString() + "/" + gameManager.GetEnemyCharacter.TotalHealth;
                if (!gameManager.GetEnemyCharacter.IsDead) //if the enemy is not dead it will attack
                {
                    gameManager.GetPlayerCharacter.TakeDamage(gameManager.GetEnemyCharacter.CalcTotalAttackValue() - gameManager.GetPlayerCharacter.CalcTotalDefenseValue());
                    playerHealthDisplay.Text = gameManager.GetPlayerCharacter.CurrentHealth.ToString() + "/" + gameManager.GetPlayerCharacter.TotalHealth;
                }
            }
            else if (gameManager.GetPlayerCharacter.IsDead) //if player is dead then show loose screen
            {
                var looseScreen = new looserScreen();
                looseScreen.Location = this.Location;
                looseScreen.Show();
                this.Close();

            }
            else if (gameManager.GetEnemyCharacter.IsDead) //if enemy is dead then spawn a new one and update the info to the new enemy.
            {
                gameManager.NewEnemy();
                enemyHealthDisplay.Text = gameManager.GetEnemyCharacter.CurrentHealth.ToString() + "/" + gameManager.GetEnemyCharacter.TotalHealth;
                playerHealthDisplay.Text = gameManager.GetPlayerCharacter.CurrentHealth.ToString() + "/" + gameManager.GetPlayerCharacter.TotalHealth;
                enemyNameLabel.Text = gameManager.GetEnemyCharacter.Name;
            }
        }
    }
}
