﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    class PokingStick : Item, IWeapon
    {
        private string _name = "Poking Stick";
        private InventorySlotId _slot = InventorySlotId.WEAPON;
        private bool _isNatural = false;
        private int _weaponAttackValue = 5;

        public int AttackValue { get { return _weaponAttackValue; } }
    }
}
