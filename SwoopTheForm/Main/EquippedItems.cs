﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    class EquippedItems
    {
        private Item[] _slots;

        public EquippedItems()
        {
            _slots = new Item[7];
        }

        //public Item GetItem(InventorySlotId slot)
        //{

        //}

        //public Item Equip(InventorySlotId slot, Item item)
        //{
        //    _slots[slot] = item;
        //}

        //public Item Unequip(InventorySlotId slot)
        //{

        //}

        public double CalcTotalWeight()
        {
            double weight = 0;
            for (int i = 0; i < _slots.Length; i++)
            {
                weight += _slots[i].Weight;
            }
            return weight;
        }

        //public int CalcTotalAttackValue()
        //{

        //}

        //public int CalcTotalDefenseValue()
        //{

        //}
    }
}
