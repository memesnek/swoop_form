﻿namespace Main
{
    partial class looserScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.looserLabel = new System.Windows.Forms.Label();
            this.retryBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // looserLabel
            // 
            this.looserLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.looserLabel.AutoSize = true;
            this.looserLabel.Font = new System.Drawing.Font("Monotype Corsiva", 72F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.looserLabel.Location = new System.Drawing.Point(183, 9);
            this.looserLabel.Name = "looserLabel";
            this.looserLabel.Size = new System.Drawing.Size(275, 117);
            this.looserLabel.TabIndex = 0;
            this.looserLabel.Text = "Looser";
            // 
            // retryBtn
            // 
            this.retryBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retryBtn.Font = new System.Drawing.Font("Monotype Corsiva", 27.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.retryBtn.Location = new System.Drawing.Point(203, 296);
            this.retryBtn.Name = "retryBtn";
            this.retryBtn.Size = new System.Drawing.Size(255, 117);
            this.retryBtn.TabIndex = 1;
            this.retryBtn.Text = "no u I\'m trying again";
            this.retryBtn.UseVisualStyleBackColor = true;
            this.retryBtn.Click += new System.EventHandler(this.retryBtn_Click);
            // 
            // looserScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.ClientSize = new System.Drawing.Size(667, 453);
            this.Controls.Add(this.retryBtn);
            this.Controls.Add(this.looserLabel);
            this.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "looserScreen";
            this.Text = "looserScreen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label looserLabel;
        private System.Windows.Forms.Button retryBtn;
    }
}