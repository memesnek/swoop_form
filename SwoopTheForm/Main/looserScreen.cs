﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class looserScreen : Form
    {
        public looserScreen()
        {
            InitializeComponent();
        }

        private void retryBtn_Click(object sender, EventArgs e)
        {
            //create a new fight screen and display it
            var fightScreen = new FightScreen();
            fightScreen.Show();
            this.Close();
        }
    }
}
